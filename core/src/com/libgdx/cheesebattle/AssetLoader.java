package com.libgdx.cheesebattle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Luksor on 2015-11-17.
 */
public class AssetLoader {
    private Map<String, Sprite> sprites = new HashMap<String, Sprite>();
    private Map<String, Sound> sounds = new HashMap<String, Sound>();
    private Texture tSprites;
    private Texture tLogo;

    public AssetLoader() {
        tSprites = new Texture("sprites.png");
        tLogo = new Texture("logo.png");

        sprites.put("playerStart", new Sprite(tSprites, 768, 750, 100, 100));
        sprites.put("gameSetup", new Sprite(tSprites, 789, 212, 640, 201));
        sprites.put("firstMenuBg", new Sprite(tSprites, 789, 0, 640, 217));
        sprites.put("logo", new Sprite(tLogo, 0, 0, 640, 256));
        sprites.put("bg", new Sprite(tSprites, 0, 0, 725, 725));
        sprites.put("arrow", new Sprite(tSprites, 725, 0, 64, 64));
        sprites.put("cheese", new Sprite(tSprites, 725, 64, 64, 64));
        sprites.put("finish", new Sprite(tSprites, 725, 384, 64, 64));
        sprites.put("betweenRoundButton1", new Sprite(tSprites, 789, 413, 212, 98));
        sprites.put("betweenRoundButton2", new Sprite(tSprites, 789+214, 413, 212, 98));
        sprites.put("betweenRoundButton3", new Sprite(tSprites, 789+213+216, 413, 225, 98));
        sprites.put("finishAgainButton", new Sprite(tSprites, 789, 512, 158, 98));
        sprites.put("finishExitButton", new Sprite(tSprites, 789+158, 512, 160, 98));

        sprites.put("cheeseExtraThrow", new Sprite(tSprites, 0, 853, 296, 168));
        sprites.put("cheeseFireLaser", new Sprite(tSprites, 296, 853, 296, 168));

        for (int i = 0; i < 4; i++) {
            sprites.put("rocket" + i, new Sprite(tSprites, 725, 128 + i * 64, 64, 64));
        }
        for (int i = 0; i < 6; i++) {
            sprites.put("dice" + i, new Sprite(tSprites, i * 128, 725, 128, 128));
        }

        FileHandle invalid_move = Gdx.files.internal("invalid_move.wav");
        sounds.put("invalid_move", Gdx.audio.newSound(invalid_move));

        FileHandle click = Gdx.files.internal("click.wav");
        sounds.put("click", Gdx.audio.newSound(click));

        FileHandle correct_move = Gdx.files.internal("correct_move.wav");
        sounds.put("correct_move", Gdx.audio.newSound(correct_move));
    }

    public Sprite getSprite(String sprite) {
        return sprites.get(sprite);
    }
    public Sound getSound(String sound) {
        return sounds.get(sound);
    }
}
