package com.libgdx.cheesebattle;

import com.badlogic.gdx.graphics.Color;

import java.util.Random;

/**
 * Created by Luksor on 2015-11-15.
 */
public class Board {
    private byte[][] rotation = new byte[][]{
            {0,3,3,3,3,3,3,2},
            {0,3,3,3,3,3,3,3},
            {0,0,0,0,0,3,0,3},
            {0,0,1,3,3,3,2,3},
            {0,0,1,0,0,0,0,3},
            {0,0,1,0,0,0,1,3},
            {0,1,1,1,1,1,1,2},
            {0,1,1,1,1,1,1,3}
    };

    private boolean[][] cheese = new boolean[8][8];

    private Color[][] glow = new Color[8][8];

    public int getRotation(int x, int y){
        return rotation[x][y];
    }

    public boolean hasCheese(int x, int y) {
        return cheese[x][y];
    }

    public void spawnCheese(){
        Random r = new Random();
        int x = r.nextInt(6) + 1;
        int y = r.nextInt(6) + 1;
        while(hasCheese(x, y)){
            x = r.nextInt(6) + 1;
            y = r.nextInt(6) + 1;
        }
        System.out.println(x + " " + y);
        cheese[x][y] = true;
    }

    public void removeCheese(){
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                cheese[i][j] = false;
            }
        }
    }

    public void setGlow(int x, int y, Color c) {
        if((x > 7) || (x < 0)) return;
        if((y > 7) || (y < 0)) return;
        glow[x][y] = c;
    }

    public Color getGlow(int x, int y) {
        if(glow[x][y] == null) return new Color(0, 0, 0, 0);
        return glow[x][y];
    }

    public void removeGlow() {
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                glow[i][j] = null;
            }
        }
    }
}
