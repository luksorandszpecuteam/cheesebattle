package com.libgdx.cheesebattle;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.util.ArrayList;

/**
 * Created by Luksor on 2015-11-15.
 */
public class Player {
    private Color[] playerColors = { new Color(1,0,1,0), new Color(1,0,0,0), new Color(0,1,0,0), new Color(0,0,1,0) };
    private int id;
    private int x, y;
    private float animX, animY, animStartX, animStartY, animEndX, animEndY;

    class AnimPoint{
        public int x;
        public int y;
        public int r;
    }
    private ArrayList<AnimPoint> animPoints = new ArrayList<>();

    private float animProgress = 0;
    private boolean isHuman = true;
    private boolean visible = false;
    private boolean animating = false;
    private int animPoint = 0;
    private String name = "";
    private int rotation = 0;
    private Board board;
    private CheeseBattle game;
    private int score = 0;
    private AssetLoader al;

    Player(int id, int x, int y, boolean isHuman, String name, Board board, CheeseBattle game, AssetLoader al){
        this.x = x;
        this.y = y;
        this.isHuman = isHuman;
        this.name = name;
        this.id = id;
        this.board = board;
        this.game = game;
        this.al = al;
    }

    public void move(int diceResult) {
        int newX = x;
        int newY = y;
        int diceResultTip = diceResult;

        animPoints.clear();

        int rot = board.getRotation(newX, newY);
        while (diceResult > 0) {
            switch (rot) {
                case 0:
                    newY++;
                    break;
                case 1:
                    newX--;
                    break;
                case 2:
                    newY--;
                    break;
                case 3:
                    newX++;
                    break;
            }
            board.setGlow(newX, newY, getColor());
            AnimPoint ap = new AnimPoint();
            ap.x = newX;
            ap.y = newY;
            ap.r = rot;
            animPoints.add(ap);
            diceResult--;

            if(diceResult == 0) {
                for (Player p : game.getPlayers()) {
                    if ((p.getPosX() == newX) && (p.getPosY() == newY)){
                        rot = board.getRotation(newX, newY);
                        diceResult++;
                    }
                }
            }
        }

        if (newX >= 0 && newX <= 7 && newY >= 0 && newY <= 7) {
            if(board.hasCheese(newX, newY)) {
                game.chooseCheeseAction(getID());
                game.showTip(getName() + " has thrown a " + diceResultTip + " and landed on a Cheese Power Square " + newX + "," + newY + ".", false);
            }else{
                game.showTip(getName() + " has thrown a " + diceResultTip + " and moves on to square " + newX + "," + newY + ".", false);
            }
            animStartX = x * 64;
            animStartY = y * 64;
            animEndX = animPoints.get(0).x * 64;
            animEndY = animPoints.get(0).y * 64;
            animating = true;
            animProgress = 0;
            animPoint = 0;
            setPos(newX, newY);
            al.getSound("correct_move").play();
        } else {
            board.removeGlow();
            al.getSound("invalid_move").play();
            game.showTip(getName() + " has thrown a " + diceResultTip + " - would end up outside of the board!", false);
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setPos(int nX, int nY) {
        x = nX;
        y = nY;
        animX = nX*64;
        animY = nY*64;
    }

    public int getPosX(){
        return x;
    }

    public int getPosY(){
        return y;
    }

    public void tick() {
        if (animating) {
            if (animProgress >= 1) {
                if (animPoint == animPoints.size()-1) {
                    animX = x * 64;
                    animY = y * 64;
                    animating = false;
                    board.removeGlow();
                    setRotation(board.getRotation(x, y));
                    //if(getName().equals("GG")) setPos(7,7);
                    return;
                }else{
                    rotation = animPoints.get(animPoint+1).r;
                    animStartX = animPoints.get(animPoint).x * 64;
                    animStartY = animPoints.get(animPoint).y * 64;
                    animEndX = animPoints.get(animPoint+1).x * 64;
                    animEndY = animPoints.get(animPoint+1).y * 64;
                    animProgress = 0;
                    animPoint++;
                }
            }
            animProgress += 0.05;

            animX = animStartX + ((animEndX - animStartX) * animProgress);
            animY = animStartY + ((animEndY - animStartY) * animProgress);
        }
    }

    public float getAnimX() { return animX; }

    public float getAnimY() { return animY; }

    public String getName() { return name; }

    public Color getColor() { return playerColors[id]; }

    public int getID() { return id; }

    public int getRotation() { return rotation; }

    public void setRotation(int r) {
        rotation = r;
        getSprite().setRotation(r * 90);
    }

    public boolean isHuman() { return isHuman; }

    public void show() { visible = true; }

    public void hide() { visible = false; }

    public boolean isVisible() { return visible; }

    public boolean isAnimating() { return animating; }

    public Sprite getSprite() { return al.getSprite("rocket" + getID()); }

}
