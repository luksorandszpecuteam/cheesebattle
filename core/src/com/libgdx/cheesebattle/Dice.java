package com.libgdx.cheesebattle;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import java.util.Random;
/**
 * Created by Luksor on 2015-11-17.
 */
public class Dice {
    private int diceResult = 1;
    private float diceSpeed;
    private int diceTick;
    private boolean animateDice = false;
    private Random r = new Random();
    private Sound s;
    private Player rollingPlayer;
    private boolean rollingFinished = true;

    public Dice() {
        FileHandle f = Gdx.files.internal("dice.wav");
        s = Gdx.audio.newSound(f);
    }

    public void roll(Player p) {
        rollingPlayer = p;
        diceResult = r.nextInt(6) + 1;
        diceSpeed = 1;
        diceTick = 0;
        animateDice = true;
        rollingFinished = false;
    }

    public void tick() {
        if(animateDice) {
            diceTick++;
            if(diceTick > diceSpeed){
                s.play();
                diceResult++;
                diceTick = 0;
            }
            if(diceResult > 6) diceResult = 1;
            diceSpeed+=0.02*diceSpeed;
        }
        if((diceSpeed > 80) && (!rollingFinished)) {
            animateDice = false;
            rollingPlayer.move(diceResult);
            rollingFinished = true;
        }
    }

    public boolean ready() {
        return !animateDice;
    }

    public int getValue() {
        return diceResult;
    }
}
