package com.libgdx.cheesebattle;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class CheeseBattle extends ApplicationAdapter {
    private enum State {STARTSC, MENU, INGAME, BETWEEN, FINISH}
    private static boolean DEBUG_MODE = false;
    private ShapeRenderer sr;
    private SpriteBatch batch;
    private AssetLoader al;
    private Board board;
    private Dice dice;
    private float r;
    private State state = State.STARTSC;
    private String menuItem;
    private int numPlayers = 1;
    private int numBots = 1;
    private int movingPlayer = 0;
    private int cooldown = 60 * 2;
    private float endTimer = 0;
    private boolean waitingForThrow = true;
    private boolean gameFinished = false;
    private BitmapFont font;
    private ArrayList<Player> players = new ArrayList<Player>();
    private float timerHelp;
    private int fadeIn = 300;
    private String[] plNames = { "PINK", "RED", "GREEN", "BLUE" };
    private boolean captureKeys = false;
    private int plNameMod = 0;
    private boolean lastKeys[] = new boolean[68];
    private int lastKeyTimeOut = 0;
    private int messageTicksLeft = 0;
    private int roundLimit = 1;
    private int round = 1;
    private int tipTime = 0;
    private String tipText = "";
    private Random rand = new Random();
    private Player plAttacker, plTarget;
    private boolean laserFired = false;
    private long startTime = System.currentTimeMillis();
    private long elapsedTime = 0L;
    public boolean bonusThrow = false;
    public boolean aimLaser = false;

    @Override
    public void create() {
        sr = new ShapeRenderer();
        batch = new SpriteBatch();
        r = 0;
        al = new AssetLoader();
        font = new BitmapFont();
        dice = new Dice();
        board = new Board();
        al.getSprite("finish").setPosition(7 * 64, 7 * 64);
        al.getSprite("finish").setAlpha(0.9f);
        al.getSprite("bg").setPosition(-106.5f, -106.5f);
        al.getSprite("arrow").setAlpha(1.0f);
        Arrays.fill(lastKeys, false);
    }

    public ArrayList<Player> getPlayers(){
        return players;
    }

    public void tick() {
        Player p = players.get(movingPlayer);
        elapsedTime = (new Date()).getTime() - startTime;

        if ((cooldown > 0) && (cooldown != -1)) cooldown--;
        if (aimLaser) {
            board.removeGlow();
            ArrayList<Player> playersAvailable = new ArrayList<>();
            for(Player tp : players){
                if(!tp.isVisible()) continue;
                if(p.equals(tp)) continue;
                if(p.isHuman()) {
                    int pX = tp.getPosX() * 64;
                    int pY = tp.getPosY() * 64;
                    int mX = Gdx.input.getX();
                    int mY = 512 - Gdx.input.getY();
                    if ((mX > pX) && (mX < pX + 64) && (mY > pY) && (mY < pY + 64)) {
                        board.setGlow(tp.getPosX(), tp.getPosY(), new Color(1, 1, 1, 1));
                        if (leftButtonPressed()) {
                            aimLaser = false;
                            fireLaser(p, tp);
                            board.removeGlow();
                        }
                    }
                }
                else {
                    playersAvailable.add(tp);
                }
            }
            if(!p.isHuman()) {
                Player randomTarget = playersAvailable.get(rand.nextInt(playersAvailable.size()));
                aimLaser = false;
                showTip(p.getName() + " fired at " + randomTarget.getName() + ".", false);
                fireLaser(p, randomTarget);
                board.removeGlow();
            }
            return;
        }
        if (waitingForThrow) { //Waiting for player to throw the die
            if (cooldown == 0) {
                p.show(); //Make the player visible

                boolean rollDice = false; //Assume no one have tried to throw the die
                if (p.isHuman()) {
                    if (leftButtonPressed()) rollDice = true; //If player is a human, mouse click is required
                } else {
                    rollDice = true; //For CPU players just throw it instantly
                }
                if (rollDice) {
                    waitingForThrow = false; //No longer waiting for throw
                    dice.roll(p); //Start rolling the die
                }
            }
        }else {
            if (!p.isAnimating() && dice.ready() && cooldown != -1) {
                waitingForThrow = true;
                if (p.getPosX() == 7 && p.getPosY() == 7 && !gameFinished) { //If the player reached the final square
                    System.out.println(p.getName() + " won the game!");
                    p.setScore(p.getScore() + 1);
                    gameFinished = true;
                    if (round < roundLimit) {
                        state = State.BETWEEN;
                    }else{
                        state = State.FINISH;
                    }
                    round++;
                } else {
                    if(bonusThrow) {
                        bonusThrow = false;
                    }else {
                        movingPlayer++;
                        if (numPlayers + numBots == movingPlayer) movingPlayer = 0;
                    }
                    if (cooldown == 0) cooldown = 30;
                }
            }
        }
    }

    public void fireLaser(Player plAttacker, Player plTarget){
        cooldown = 200;
        laserFired = true;
        this.plAttacker = plAttacker;
        this.plTarget = plTarget;
    }

    public void restart(State s) {

        if(s == State.INGAME) {
            //Reset players
            for (Player p : players) {
                p.setPos(0, 0);
                p.setRotation(0);
                p.hide();
            }
            board.removeCheese();
            for (int i = 0; i < 4; i++) {
                board.spawnCheese();
            }
        }else{
            round = 1;
            players.clear();
        }

        for(int i = 0; i < 4; i++) {
            al.getSprite("rocket" + i).setRotation(0);
        }

        //Reset variables
        state = s;
        movingPlayer = 0;
        cooldown = 60 * 2;
        endTimer = 0;
        gameFinished = false;
        fadeIn = 300;
        captureKeys = false;
        plNameMod = 0;
        lastKeyTimeOut = 0;
        messageTicksLeft = 0;
        tipTime = 0;
    }

    public void showMessageAnimation(int cd){
        cooldown = cd;
        messageTicksLeft = 300;
    }

    public void showTip(String txt, boolean stay){
        if(stay) {
            tipTime = -1;
        }else{
            tipTime = 60*3;
        }
        tipText = txt;
    }

    public void chooseCheeseAction(int plID){
        showMessageAnimation(-1);
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        switch (state) {
            case STARTSC:
                drawStartScreen();
                break;
            case BETWEEN:
                drawBetweenScreen();
                break;
            case FINISH:
                drawFinishScreen();
                break;
            case MENU:
                drawMenu();
                break;

            default:
                tick();

                Sprite bg = al.getSprite("bg");
                bg.setScale(1,1);

                if (r >= 360) r = 0;
                al.getSprite("bg").setRotation(r);
                r += 0.04;

                batch.begin();
                al.getSprite("bg").draw(batch);
                al.getSprite("finish").draw(batch);
                batch.end();
                drawBoard();

                int seconds = (int) (elapsedTime / 1000) % 60 ;
                int minutes = (int) ((elapsedTime / (1000*60)) % 60);

                //Draw a black background on the right side
                sr.begin(ShapeRenderer.ShapeType.Filled);
                sr.setColor(0, 0, 0, 0);
                sr.rect(512, 0, 256, 512);
                sr.end();

                //Draw round counter
                batch.begin();
                font.setColor(Color.WHITE);
                font.draw(batch,"Round  " + round + " / " + roundLimit, 512+43, 20);

                if(seconds<10){
                    font.draw(batch,(minutes + " : 0"+seconds),512+85, 40);
                }
                else{
                    font.draw(batch,(minutes + " : "+seconds),512+85, 40);
                }

                batch.end();

                batch.begin();
                int i = 0;
                for (Player p : players) {

                    //Update players
                    if (p.isVisible()) {
                        p.tick();
                        Sprite rocket = p.getSprite();
                        rocket.setPosition(p.getAnimX(), p.getAnimY());
                        rocket.setRotation(p.getRotation() * 90);
                        rocket.draw(batch);
                    }

                    Color plColor = p.getColor();
                    plColor.a = (movingPlayer == p.getID()) ? 1 : 0.5f;
                    font.setColor(plColor);

                    font.draw(batch, p.getName(), 512 + 3, (512 - 128 - 10) - (16 * i));
                    i++;
                }
                dice.tick();
                batch.draw(al.getSprite("dice" + (dice.getValue() - 1)), 512, 512 - 128);
                batch.end();

                if (laserFired) {
                    plTarget.getSprite().setScale(cooldown / 200f);

                    //Draw the laser
                    batch.begin();
                    sr.begin(ShapeRenderer.ShapeType.Filled);
                    sr.setColor(1, 0, 0, 1);
                    sr.rectLine(plAttacker.getAnimX() + 32, plAttacker.getAnimY() + 32, plTarget.getAnimX() + 32, plTarget.getAnimY() + 32, 4f);
                    sr.end();
                    batch.end();

                    //Finish animation
                    if(cooldown == 0) {
                        plTarget.getSprite().setScale(1f);
                        laserFired = false;
                        plTarget.setRotation(0);
                        plTarget.setPos(0, 0);
                    }
                }

                if (fadeIn > 0) {
                    fadeIn--;
                    Gdx.gl.glEnable(GL20.GL_BLEND);
                    batch.begin();
                    sr.begin(ShapeRenderer.ShapeType.Filled);
                    sr.setColor(0, 0, 0, fadeIn / (300.0f));
                    sr.rect(0, 0, 512 + 128, 512);
                    sr.end();
                    batch.end();
                    Gdx.gl.glDisable(GL20.GL_BLEND);
                }

                if (tipTime > 0){
                    if(tipTime != -1) tipTime--;

                    float H,PL;
                    float P = 1f-((tipTime*100f)/180f/100f);

                    boolean drawContent = false;

                    if(P<0.05){
                        PL = P*20;
                        H = 20*PL;
                    }else if(P<0.95){
                        H = 20;
                        drawContent = true;
                    }else{
                        PL = 1f-((P-0.95f)*20f);
                        H = 20*PL;
                    }

                    Gdx.gl.glEnable(GL20.GL_BLEND);
                    batch.begin();
                    sr.begin(ShapeRenderer.ShapeType.Filled);
                    sr.setColor(0f, 0f, 0f, 0.8f);
                    sr.rect(0, 0, 512, H);
                    sr.end();
                    batch.end();
                    Gdx.gl.glDisable(GL20.GL_BLEND);

                    if(drawContent) {
                        batch.begin();
                        font.setColor(Color.WHITE);
                        font.draw(batch, tipText, 4, 16);
                        batch.end();
                    }
                }

                if (messageTicksLeft > 0){
                    messageTicksLeft--;
                    if(messageTicksLeft > 150) break;

                    float H,Y,PL;
                    float P = 1f-((messageTicksLeft*100f)/150f/100f);

                    boolean drawContent = false;

                    if(P<0.05){
                        PL = P*20;
                        H = 200*PL;
                    }else if(P<0.95){
                        H = 200;
                        drawContent = true;
                        if(cooldown == -1) messageTicksLeft++;
                    }else{
                        PL = 1f-((P-0.95f)*20f);
                        H = 200*PL;
                    }

                    Gdx.gl.glEnable(GL20.GL_BLEND);
                    batch.begin();
                    sr.begin(ShapeRenderer.ShapeType.Filled);
                    sr.setColor(1f, 1f, 1f, 0.8f);
                    Y = (512/2) - (H/2.0f);
                    sr.rect(0, Y, 512 + 128, H);
                    sr.end();
                    batch.end();
                    Gdx.gl.glDisable(GL20.GL_BLEND);

                    if(drawContent) {
                        batch.begin();
                        Sprite cheeseExtraThrow = al.getSprite("cheeseExtraThrow");
                        Sprite cheeseFireLaser = al.getSprite("cheeseFireLaser");
                        cheeseExtraThrow.setPosition(16, 156 + 16);
                        cheeseFireLaser.setPosition(16 + 296 + 16, 156 + 16);
                        cheeseExtraThrow.draw(batch);
                        cheeseFireLaser.draw(batch);
                        batch.end();
                        if(bonusThrow || aimLaser || cooldown > 0) break;
                        cheeseExtraThrow.setAlpha(0.8f);
                        cheeseFireLaser.setAlpha(0.8f);
                        if(players.get(movingPlayer).isHuman()) {
                            nextMenuItem("btnExtraThrow", 16, 156 + 16, 296, 168);
                            nextMenuItem("btnFireLaser", 16 + 296 + 16, 156 + 16, 296, 168);
                            if (menuItem.equals("btnExtraThrow")) {
                                cheeseExtraThrow.setAlpha(1.0f);
                                if (leftButtonPressed()) {
                                    messageTicksLeft = 30;
                                    cooldown = 50;
                                    bonusThrow = true;
                                    showTip(players.get(movingPlayer).getName() + " has chosen an extra throw.", false);
                                    cheeseFireLaser.setAlpha(0.3f);
                                }
                            } else {
                                cheeseExtraThrow.setAlpha(0.8f);
                            }
                            if (menuItem.equals("btnFireLaser")) {
                                cheeseFireLaser.setAlpha(1.0f);
                                if (leftButtonPressed()) {
                                    messageTicksLeft = 30;
                                    cooldown = 50;
                                    aimLaser = true;
                                    showTip("Click on a player you want to attack.", false);
                                    cheeseExtraThrow.setAlpha(0.3f);
                                }
                            } else {
                                cheeseFireLaser.setAlpha(0.8f);
                            }
                        }else{
                            int action = rand.nextInt(2);
                            if(action == 0) {
                                bonusThrow = true;
                                showTip(players.get(movingPlayer).getName() + " has chosen an extra throw.", false);
                                cheeseFireLaser.setAlpha(0.3f);
                            }else{
                                aimLaser = true;
                                cheeseExtraThrow.setAlpha(0.3f);
                            }
                            messageTicksLeft = 70;
                            cooldown = 100;
                        }
                    }
                }
                break;
        }
    }

    public void drawStartScreen() {
        batch.begin();
        Sprite bg = al.getSprite("bg");
        bg.setAlpha(0.5f);
        bg.draw(batch);
        bg.scale(0.0005f);

        Sprite firstMenuBg = al.getSprite("firstMenuBg");
        firstMenuBg.draw(batch);
        firstMenuBg.setAlpha(0.6f);

        Sprite logo = al.getSprite("logo");
        logo.setPosition(0, 512 - 256);
        logo.draw(batch);

        font.draw(batch,"Credits: Lukasz Kaleta, Piotr Bryla",415,20);
        font.setColor(0,0,0,0.6f);

        batch.end();

        if (Gdx.input.getY() > 290) {
            al.getSprite("firstMenuBg").setAlpha(1.0f);
            font.setColor(0,0,0,0.9f);
            if (Gdx.input.isTouched()) {
                state = State.MENU;
                al.getSound("click").play();
            }
        }
    }

    public void drawMenu() {
        Sprite playerStart = al.getSprite("playerStart");
        Sprite gameSetup = al.getSprite("gameSetup");
        gameSetup.setPosition(0, 512 - 203);
        Sprite bg = al.getSprite("bg");

        font.setColor(Color.WHITE);
        menuItem = "";

        batch.begin();

        bg.setScale(1.3f,1.3f);
        bg.draw(batch);

        batch.end();

        batch.begin();
        float div = ((512 + 128) / 4);

        for (int i = 0; i < 4; i++) {
            sr.begin(ShapeRenderer.ShapeType.Filled);
            sr.setColor(0.2f, 0.2f, 0.2f, 1f);
            sr.rect((div - 64) / 2 + i * div, 200, 64, 64);
            sr.end();
        }
        batch.end();

        for (int i = 0; i < numBots + numPlayers; i++) {
            nextMenuItem("setName" + i, Math.round((div - 64) / 2 + i * div) - 30, 190 - 16, 130, 20);
            batch.begin();
            if ((i == plNameMod) && captureKeys) {
                font.setColor(Color.YELLOW);
                font.draw(batch, plNames[i] + "|", (div - 64) / 2 + (i * div) + 30 - plNames[i].length() * 4, 190);
            } else {
                if (menuItem.equals("setName" + i)) {
                    font.setColor(Color.YELLOW);
                } else {
                    font.setColor(Color.WHITE);
                }
                font.draw(batch, plNames[i], (div - 64) / 2 + (i * div) + 30 - plNames[i].length() * 4, 190);
            }
            batch.end();
        }

        nextMenuItem("humanPlayersBtn", 100, 150 - 16, 150, 16);
        nextMenuItem("cpuPlayersBtn", 400, 150 - 16, 150, 16);
        nextMenuItem("playBtn", 0, 0, 100, 100);
        nextMenuItem("roundPlusButton", (640 / 2) + 35, 95 - 15, 35, 19);
        nextMenuItem("roundMinusButton", (640 / 2) - 70, 95 - 15, 35, 19);

        batch.begin();

        Gdx.gl.glEnable(GL20.GL_BLEND);
        sr.begin(ShapeRenderer.ShapeType.Filled);

        if (menuItem == "roundPlusButton") {
            sr.setColor(0, 0, 255, 0.7f);
        } else {
            sr.setColor(192, 192, 192, 0.4f);
            }
        sr.rect((640 / 2) + 35, 95 - 15, 35, 19);

        if (menuItem == "roundMinusButton") {
            sr.setColor(255, 0, 0, 0.7f);
        } else {
            sr.setColor(192, 192, 1192, 0.4f);
            }
        sr.rect((640 / 2) - 70, 95 - 15, 35, 19);

        sr.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);

        batch.end();
        batch.begin();

        font.setColor((menuItem.equals("humanPlayersBtn")) ? Color.YELLOW : Color.WHITE);
        font.draw(batch, "Human players: " + numPlayers, 100, 150);

        font.setColor((menuItem.equals("cpuPlayersBtn")) ? Color.YELLOW : Color.WHITE);
        font.draw(batch, "Computer players: " + numBots, 400, 150);

        font.setColor(Color.WHITE);
        if ( (numPlayers + numBots < 2) || plNames[0].length() == 0 || plNames[1].length() == 0 || plNames[2].length() == 0 || plNames[3].length() == 0 ) {
            font.setColor(Color.DARK_GRAY);
            playerStart.setAlpha(0.35f);
        } else {
            if (menuItem.equals("playBtn")) {
                playerStart.setAlpha(1.0f);
            }else {
                playerStart.setAlpha(0.8f);
            }
        }

        playerStart.draw(batch);
        gameSetup.draw(batch);

        for (int i = 0; i < numBots + numPlayers; i++) {
            font.setColor(Color.GRAY);
            if(i < numPlayers) {
                font.draw(batch, "HUMAN", (div - 64) / 2 + (i * div) + 7, 200 + 80);
            }else{
                font.draw(batch, "CPU", (div - 64) / 2 + (i * div) + 17, 200 + 80);
            }
            Sprite rocket = al.getSprite("rocket" + i);
            div = ((512 + 128) / 4);
            rocket.setPosition((div - 64) / 2 + i * div, 200);
            rocket.draw(batch);
        }
        font.setColor(Color.WHITE);
        font.draw(batch,""+ roundLimit,(640/2)-5,115);
        font.draw(batch,"ROUNDS",(640/2)-30,95);

        font.setColor((menuItem.equals("roundMinusButton")) ? Color.YELLOW : Color.WHITE);
        font.draw(batch,"-",((640/2)-28)-25,97);

        font.setColor((menuItem.equals("roundPlusButton")) ? Color.YELLOW : Color.WHITE);
        font.draw(batch,"+",((640/2)+30)+20,95);

        batch.end();

        if(captureKeys){
            if(lastKeyTimeOut > 0) {
                lastKeyTimeOut--;
            }else{
                Arrays.fill(lastKeys, false);
            }
            if(Gdx.input.isKeyPressed(Input.Keys.ENTER)){
                captureKeys = false;
                return;
            }
            if(Gdx.input.isKeyPressed(Input.Keys.BACKSPACE)){
                if(lastKeys[Input.Keys.BACKSPACE]) return;
                String s = plNames[plNameMod];
                if(s.length() > 0) s = s.substring(0, s.length()-1);
                plNames[plNameMod] = s;
                lastKeyTimeOut = 5;
                lastKeys[Input.Keys.BACKSPACE] = true;
                return;
            }
            if(plNames[plNameMod].length() >= 10) return;
            if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
                if(lastKeys[Input.Keys.SPACE]) return;
                plNames[plNameMod] += " ";
                lastKeyTimeOut = 10;
                lastKeys[Input.Keys.SPACE] = true;
                return;
            }
            for(int i = 29; i <= 54; i++){
                if(Gdx.input.isKeyPressed(i)){
                    if(lastKeys[i]) continue;
                    plNames[plNameMod] += (char)(i+36);
                    lastKeyTimeOut = 10;
                    lastKeys[i] = true;
                }
            }
        }

        if (!leftButtonPressed()) return;
        if (menuItem == null) return;
        switch (menuItem) {
            case "setName0":
                switchEdit(0);
                break;

            case "setName1":
                switchEdit(1);
                break;

            case "setName2":
                switchEdit(2);
                break;

            case "setName3":
                switchEdit(3);
                break;

            case "humanPlayersBtn":
                if (numPlayers == 4 - numBots) numPlayers = -1;
                numPlayers++;
                al.getSound("click").play();
                break;

            case "cpuPlayersBtn":
                if (numBots == 4 - numPlayers) numBots = -1;
                numBots++;
                al.getSound("click").play();
                break;

            case "playBtn":
                if ( (numPlayers + numBots < 2) || plNames[0].length() == 0 || plNames[1].length() == 0 || plNames[2].length() == 0 || plNames[3].length() == 0 ) break;
                state = State.INGAME;
                al.getSound("click").play();
                startTime = System.currentTimeMillis();

                board.removeCheese();
                for (int i = 0; i < 4; i++) {
                    board.spawnCheese();
                }
                for (int i = 0; i < numPlayers; i++) {
                    players.add(new Player(i, 0, 0, true, plNames[i], board, this, al));
                    System.out.println(i);
                }
                for (int i = 0; i < numBots; i++) {
                    players.add(new Player(numPlayers + i, 0, 0, false, plNames[numPlayers + i], board, this, al));
                    System.out.println(numPlayers + i);
                }
                break;

            default:
                captureKeys = false;
                break;
        }

        //Rounds Set min = 1, max = 5

        if (menuItem == null) return;
        if (leftButtonPressed()) return;
        switch (menuItem) {
            case "roundPlusButton":
                if(roundLimit < 5) {
                    al.getSound("click").play();
                    roundLimit++;
                } else {
                    al.getSound("click").play();
                    roundLimit = 1;
                }
                break;

            case "roundMinusButton":
                if(roundLimit > 1){
                    al.getSound("click").play();
                    roundLimit--;
                } else {
                    al.getSound("click").play();
                    roundLimit = 5;
                }
                break;

            default:
                break;
        }
    }

    public void switchEdit(int i){
        plNameMod = i;
        captureKeys = !captureKeys;
    }

    public void updateEndScreen() {
        timerHelp += Gdx.graphics.getDeltaTime();

        if ((timerHelp > 0.1f) && (endTimer < 9)) {
            System.out.println("Test " + endTimer);
            timerHelp = 0;
            endTimer += 0.3;
        }

        Sprite logo = al.getSprite("logo");
        logo.setPosition(0, 512 - 215);
        logo.setAlpha(0.0f);
        logo.setScale(0.6f,0.6f);

        Sprite bg = al.getSprite("bg");
        bg.setAlpha(0.5f);
        bg.draw(batch);
        bg.scale(0.0005f);
    }

    public void drawFinishScreen(){
        batch.begin();

        updateEndScreen();

        Sprite logo = al.getSprite("logo");

        // Buttons definitions

        Sprite again = al.getSprite("finishAgainButton");
        again.setPosition(160, 0);

        Sprite exit = al.getSprite("finishExitButton");
        exit.setPosition(160+157, 0);

        // Entry animation

        if ((endTimer < 12) && (0.1f * endTimer <= 1.0f)) {
            font.setColor(0, 0, 100, 0.1f * endTimer);

            logo.setAlpha(0.1f * endTimer);
            logo.draw(batch);

            again.setAlpha(0.07f * endTimer);
            again.draw(batch);

            exit.setAlpha(0.07f * endTimer);
            exit.draw(batch);

        } else {
            font.setColor(0,0,0,1);
            logo.setAlpha(1.0f);
            logo.draw(batch);
        }

        batch.end();

        // Table drawing
        drawRanking();

        batch.begin();

        // Button select animations and mechanics
        if(endTimer >8.7f){
            if (Gdx.input.getY() > 512-98 && Gdx.input.getX() > 160 && Gdx.input.getX() < 160+157) {
                again.setAlpha(1.0f);
                if(leftButtonPressed()){
                    al.getSound("click").play();
                    restart(State.MENU);
                }
            }

            if (Gdx.input.getY() > 512-98 && Gdx.input.getX() > 160+157 && Gdx.input.getX() <160+157+157) {
                exit.setAlpha(1.0f);
                if(leftButtonPressed()){
                    al.getSound("click").play();
                    dispose();
                    Gdx.app.exit();
                }
            }
        }

        again.draw(batch);
        exit.draw(batch);

        batch.end();
    }

    public void drawBetweenScreen() {
        batch.begin();

        updateEndScreen();

        Sprite logo = al.getSprite("logo");

        if ((endTimer < 12) && (0.1f * endTimer <= 1.0f)) {
            font.setColor(0,0,100,0.1f * endTimer);
            logo.setAlpha(0.1f * endTimer);
            logo.draw(batch);
        } else {
            font.setColor(0,0,0,1);
            logo.setAlpha(1.0f);
            logo.draw(batch);
        }
        batch.end();
        drawRanking();

        batch.begin();
        Sprite button1 = al.getSprite("betweenRoundButton1");
        Sprite button2 = al.getSprite("betweenRoundButton2");
        Sprite button3 = al.getSprite("betweenRoundButton3");

        button1.setPosition(0, 0);
        button2.setPosition(button1.getX() + 212, 0);
        button3.setPosition(button2.getX() + 212, 0);

        button1.setAlpha(0.6f);
        button2.setAlpha(0.6f);
        button3.setAlpha(0.6f);

        if (Gdx.input.getY() > 512-100 && Gdx.input.getX() < 212) {
            button1.setAlpha(1.0f);
            if(leftButtonPressed()){
                al.getSound("click").play();
                startTime = System.currentTimeMillis(); // Timer reset
                restart(State.INGAME);
            }
        }

        if (Gdx.input.getY() > 512-100 && Gdx.input.getX() > 212 && Gdx.input.getX() < 214+212) {
            button2.setAlpha(1.0f);
            if(leftButtonPressed()){
                al.getSound("click").play();
                restart(State.MENU);
            }
        }

        if (Gdx.input.getY() > 512-100 && Gdx.input.getX() > 214+212) {
            button3.setAlpha(1.0f);
            if(leftButtonPressed()){
                al.getSound("click").play();
                dispose();
                Gdx.app.exit();
            }
        }

        button1.draw(batch);
        button2.draw(batch);
        button3.draw(batch);
        batch.end();
    }

    public void drawRanking() {
        // Table Sorting

        Player[] ranking = new Player[players.size()];

        for (int i = players.size(); i > 0; i--) {
            ranking[i - 1] = players.get(i - 1);
        }
        Arrays.sort(ranking, (a, b) -> b.getScore() - a.getScore());

        // Drawing Ranking List

        int playersX = 240;
        int playersY = 275;
        int playersSeparator = 40;

        batch.begin();

        Gdx.gl.glEnable(GL20.GL_BLEND);
        sr.begin(ShapeRenderer.ShapeType.Filled);

        sr.setColor(255, 255, 255, 0.1f);
        sr.rect((playersX + 50) - 20, (playersY + 30)+10,90,30);

        sr.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);

        batch.end();
        batch.begin();

        font.setColor(Color.WHITE);
        font.draw(batch, "Ranking: ", playersX + 50, playersY+60);

        batch.end();

        for (int i = 0; i < numPlayers + numBots; i++) {
            batch.begin();

            Gdx.gl.glEnable(GL20.GL_BLEND);
            sr.begin(ShapeRenderer.ShapeType.Filled);

            sr.setColor(255, 255, 255, 0.1f);
            sr.rect(playersX-10, (playersY - playersSeparator * i)-15,200,20);

            sr.end();
            Gdx.gl.glDisable(GL20.GL_BLEND);

            batch.end();
            batch.begin();

            Color c = ranking[i].getColor();
            c.a = 1.0f;
            font.setColor(c);
            font.draw(batch, (i + 1) + ". ", playersX, playersY - playersSeparator * i); // First player string
            font.draw(batch, ranking[i].getName(), playersX + 70, playersY - playersSeparator * i);
            font.draw(batch, Integer.toString(ranking[i].getScore()), playersX + 170, playersY - playersSeparator * i);

            batch.end();
        }

    }

    public void nextMenuItem(String item, int posX, int posY, int sizeX, int sizeY) {
        batch.begin();
        sr.begin(ShapeRenderer.ShapeType.Line);
        sr.setColor(255, 0, 0, 255);

        if ((512 - Gdx.input.getY() > posY) && (512 - Gdx.input.getY() < posY + sizeY) && (Gdx.input.getX() > posX) && (Gdx.input.getX() < posX + sizeX)) {
            menuItem = item;
            sr.setColor(0, 255, 0, 255);
        }

        if (DEBUG_MODE){
            sr.rect(posX, posY, sizeX, sizeY);
        }

        sr.end();
        batch.end();
    }

    boolean lastLeft = false;

    public boolean leftButtonPressed() {
        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            if (!lastLeft) {
                lastLeft = true;
                return true;
            }
        } else {
            lastLeft = false;
        }
        return false;
    }

    public void drawBoard() {
        for (int x = 7; x >= 0; x--) {
            for (int y = 7; y >= 0; y--) {
                if (x == 7 && y == 7) continue;
                if (board.hasCheese(x, y)) {
                    batch.begin();
                    Sprite cheese = al.getSprite("cheese");
                    cheese.setPosition(x * 64, y * 64);
                    cheese.setRotation(board.getRotation(x, y) * 90);
                    cheese.draw(batch);
                    batch.end();
                }

                Gdx.gl.glEnable(GL20.GL_BLEND);
                batch.begin();
                sr.begin(ShapeRenderer.ShapeType.Filled);
                Color col = board.getGlow(x, y);
                col.a = 0.4f;
                sr.setColor(col);
                sr.rect(x*64, y*64, 64, 64);
                sr.end();
                batch.end();
                Gdx.gl.glDisable(GL20.GL_BLEND);

                batch.begin();
                Sprite arrow = al.getSprite("arrow");
                arrow.setPosition(x * 64, y * 64);
                arrow.setRotation(board.getRotation(x, y) * 90);
                arrow.draw(batch);
                batch.end();
            }
        }
    }

    @Override // Draft memory releasing
    public void dispose() {
        super.dispose();
    }
}