package com.libgdx.cheesebattle.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.libgdx.cheesebattle.CheeseBattle;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 512 + 128;
		config.height= 512;
		config.resizable = false;
		new LwjglApplication(new CheeseBattle(), config);
	}
}
